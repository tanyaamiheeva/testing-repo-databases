#! /usr/bin/env bash

python3 utils/pypsql.py --file="ddl.sql"
python3 utils/pypsql.py --csv="data/strava_user.csv" --table="hw1.strava_user"
python3 utils/pypsql.py --csv="data/strava_running_activity.csv" --table="hw1.strava_running_activity"
echo "DDL finished"
