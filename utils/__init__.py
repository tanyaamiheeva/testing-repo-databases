import os
import typing
from dataclasses import dataclass
from urllib.parse import quote

import pandas as pd
import psycopg2 as pg
import sqlalchemy


@dataclass
class TableDefinition:
    table: str
    col_names: typing.List[str]
    plain_data_file_name: str


@dataclass
class Credentials:
    dbname: str = "postgres"
    host: str = "127.0.0.1"
    port: int = 5432
    user: str = "postgres"
    password: str = "password"


def _extract_credentials() -> Credentials:
    return Credentials(
        dbname=os.getenv("DBNAME", Credentials.dbname),
        host=os.getenv("DBHOST", Credentials.host),
        port=os.getenv("DBPORT", Credentials.port),
        user=os.getenv("DBUSER", Credentials.user),
        password=os.getenv("DBPASSWORD", Credentials.password),
    )


def _psycopg2_conn_string(creds: typing.Optional[Credentials] = None) -> str:
    if not creds:
        creds = _extract_credentials()
    return f"""
        dbname={creds.dbname!r}
        user={creds.user!r}
        host={creds.host!r}
        port={creds.port!r}'
        password={creds.password!r}'
    """


def _psycopg2_conn(conn_string: typing.Optional[str] = None):
    if not conn_string:
        conn_string = _psycopg2_conn_string()
    return pg.connect(conn_string)


def _psycopg2_execute_sql(sql: str, conn=None) -> typing.NoReturn:
    if not conn:
        conn = _psycopg2_conn()
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()


def _sqlalchemy_conn_string(creds: typing.Optional[Credentials] = None) -> str:
    if not creds:
        creds = _extract_credentials()
    return (
        "postgresql://"
        f"{creds.user}:{quote(creds.password)}@"
        f"{creds.host}:{creds.port}/{creds.dbname}"
    )


def _sqlalchemy_conn(conn_string: typing.Optional[str] = None):
    if not conn_string:
        conn_string = _sqlalchemy_conn_string()
    return sqlalchemy.create_engine(conn_string)


def _execute_sql_to_df(conn, sql: str) -> pd.DataFrame:
    return pd.read_sql(sql, con=conn)


def _read_sql(filepath: str) -> str:
    with open(filepath, "r") as file:
        return file.read().rstrip()


def _copy_csv(
    filepath: str,
    table: str,
    col_names: typing.List[str],
    sep: str = ",",
    if_exists: str = "replace",
    conn=None,
) -> typing.NoReturn:
    if not conn:
        conn = _sqlalchemy_conn()

    csv_df = pd.read_csv(filepath, delimiter=sep, names=col_names, skiprows=1)

    schema = None
    if "." in table:
        schema, table = table.split(".")

    csv_df.to_sql(table, schema=schema, con=conn, if_exists=if_exists, index=False)


def _copy_csv_with_def(
    tdef: TableDefinition, sep: str = ",", if_exists: str = "replace", conn=None
) -> typing.NoReturn:
    return _copy_csv(
        filepath=tdef.plain_data_file_name,
        table=tdef.table,
        col_names=tdef.col_names,
        sep=sep,
        if_exists=if_exists,
        conn=conn,
    )
