FROM python:3.9-slim

ENV CI=1
ENV DBNAME=postgres
ENV DBHOST=host.docker.internal
ENV DBPORT=5432
ENV DBUSER=postgres
ENV DBPASSWORD=password

ARG NO_COLOUR='\033[0m'
ARG LIGHT_RED='\033[1;31m'

WORKDIR /home

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt > /dev/null 2> /dev/null
RUN echo "export PATH=$PATH:/home" >> ~/.bashrc
