create schema hw1;

create table hw1.strava_user
(
    user_id                integer,
    login                  text,
    name                   text,
    surname                text,
    registration_date      date,
    birth_date             date,
    is_subscription_active boolean,
    sex                    text
        constraint strava_user_sex_check
            check (sex = ANY (ARRAY ['male'::text, 'female'::text, NULL::text]))
);

create table hw1.strava_running_activity
(
    activity_id          integer,
    start_date_local     timestamp,
    type                 text,
    distance             double precision,
    moving_time          integer,
    elapsed_time         integer,
    total_elevation_gain double precision,
    sport_type           text,
    start_date_utc       timestamp,
    achievement_count    integer,
    kudos_count          integer,
    comment_count        integer,
    athlete_count        integer,
    photo_count          integer,
    average_speed        double precision,
    max_speed            double precision,
    user_id              integer
);
